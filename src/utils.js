/**
 * Test to see if the browser supports the HTML template element by checking
 * for the presence of the template element's content attribute.
 * @returns {boolean}
 */
const supportsTemplateTag = (() => {
  let hasSupport;

  return () => {
    if (hasSupport === undefined) {
      hasSupport = "content" in document.createElement("template");
    }

    if (hasSupport === false) {
      throw "Template tag is not supported!";
    }

    return true;
  };
})();
