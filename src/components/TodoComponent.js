/**
 * @constructor
 * @extends Component
 * @param {Todo} props
 */
function TodoComponent(props) {
  Component.call(this, props);

  this._completedEl = this.getTarget().querySelector("input.toggle");
  this.getProps().todo.completed(
    (completed) => (this._completedEl.checked = !!completed)
  );
  this._completedEl.addEventListener(
    "click",
    this._handleCompletedClick.bind(this),
    true
  );

  const textEl = this.getTarget().querySelector("label");
  this.getProps().todo.text((text) => (textEl.innerText = text));

  this._destroy = this.getTarget().querySelector("button.destroy");
  this._destroy.addEventListener("click", this.destroy.bind(this), true);
}

TodoComponent.prototype = Object.create(Component.prototype);

TodoComponent.prototype._handleCompletedClick = function () {
  this.getProps().todo.completed = this._completedEl.checked;
};

TodoComponent.prototype.destroy = function () {
  this._completedEl.removeEventListener(
    "click",
    this._handleCompletedClick.bind(this),
    true
  );
  this._destroy.removeEventListener("click", this.destroy.bind(this), true);
  Component.prototype.destroy.call(this);
};

TodoComponent.create = (() => {
  let template;

  return (props) => {
    if (!template && supportsTemplateTag()) {
      template = document.querySelector("#todo");
    }

    if (template) {
      return new TodoComponent({
        target: template.content.cloneNode(true).children[0],
        props,
      });
    } else {
      throw "Missing TodoComponent template!";
    }
  };
})();
