/**
 * @constructor
 * @extends Component
 * @param {*} props
 */
function NewTodoInputComponent(props) {
  Component.call(this, props);

  this.getTarget().addEventListener(
    "keyup",
    this._handleKeyUpEvent.bind(this),
    true
  );
}

NewTodoInputComponent.prototype = Object.create(Component.prototype);

NewTodoInputComponent.prototype._handleKeyUpEvent = function (event) {
  event.preventDefault();
  if (event.key == "Enter") {
    this._createTodo();
  }
};

NewTodoInputComponent.prototype._createTodo = function () {
  const text = this.getTarget().value;
  this.getProps().todoApp.getProps().todos.addTodo(new Todo({ text }));
  this.getTarget().value = "";
};

NewTodoInputComponent.prototype.destroy = function () {
  this.getTarget().removeEventListener(
    "keyup",
    this._handleKeyUpEvent.bind(this),
    true
  );
  Component.prototype.destroy.call(this);
};

NewTodoInputComponent.createFromTodoApp = (todoApp) => {
  const target = todoApp.getTarget().querySelector("input.new-todo");
  return new NewTodoInputComponent({ target, props: { todoApp } });
};
