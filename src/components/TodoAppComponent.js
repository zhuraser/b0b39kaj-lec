/**
 * @constructor
 * @extends Component
 * @param {{target: string|Node|Element|HTMLElement, props: {todos: object[], filter: string}}} props
 */
function TodoAppComponent(props) {
  initStores(props.props);

  Component.call(this, props);

  // init
  this._newTodoCreator = NewTodoInputComponent.createFromTodoApp(this);
  this._todoList = TodoListComponent.createFromTodoApp(this, props.props);
  this._footer = TodoAppFooterComponent.createFromTodoApp(this);

  function initStores(props) {
    for (let key of Object.keys(props)) {
      switch (key) {
        case "todos":
          props.todos = new TodoList(
            (props.todos || []).map((todo) => new Todo(todo))
          );
          break;

        default:
          props[key] = writable(props[key]);
      }
    }
  }

  this.getTarget().addEventListener(
    "clear-completed",
    clearCompletedHandler.bind(this),
    true
  );
  function clearCompletedHandler() {
    this.getProps().todos.clearCompleted();
  }
}

TodoAppComponent.prototype = Object.create(Component.prototype);
