/**
 * @constructor
 * @extends Component
 * @param {{target: string|Node|Element|HTMLElement, props: {todos: Readable<object[]>, filter: Readable<string>}}} props
 */
function TodoListComponent(props) {
  Component.call(this, props);

  // init
  this._renderedTodos = [];
  this.getProps().todos.subscribe(this._renderList.bind(this));
  this.getProps().filter.subscribe((filter) => {
    let todos = get_store_value(this.getProps().todos);

    switch (filter) {
      case "active":
        todos = todos.filter((todo) => !todo.isCompleted);
        break;
      case "completed":
        todos = todos.filter((todo) => todo.isCompleted);
        break;
    }

    this._renderList(todos);
  });
}

TodoListComponent.prototype = Object.create(Component.prototype);

/**
 * @param {Todo|TodoComponent} todo
 */
TodoListComponent.prototype._render = function (todo) {
  if (todo instanceof Todo) {
    todo = TodoComponent.create({ todo });
  }

  this._renderedTodos.push(todo);
  this.getTarget().appendChild(todo.getTarget());
};

/**
 * @param {Todo[]|TodoComponent[]} todos
 */
TodoListComponent.prototype._renderList = function (todos) {
  this._renderedTodos.forEach((todo) => todo.destroy());
  todos.forEach(this._render.bind(this));
};

/**
 * @param {TodoComponent} todo
 * @returns {TodoListComponent}
 */
TodoListComponent.prototype.append = function (todo) {
  this._render(todo);
  return this;
};

/**
 * @param {TodoAppComponent} todoApp
 * @param {{todos: Readable<object[]>, filter: Readable<string>}} props
 * @returns {TodoListComponent}
 */
TodoListComponent.createFromTodoApp = (todoApp, props) => {
  const target = todoApp.getTarget().querySelector("ul.todo-list");
  return new TodoListComponent({
    target,
    props: Object.assign({}, props, { todoApp }),
  });
};
