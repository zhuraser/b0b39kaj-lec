/**
 * @constructor
 * @extends Component
 * @param {{target: string|Node|Element|HTMLElement, props: {filter: Writable<string>, todos: Readable<Todo>}}} props
 */
function TodoAppFooterComponent(props) {
  Component.call(this, props);

  // init
  this._todoCounter = this.getTarget().querySelector(".todo-count");
  this.getProps().todos.subscribe((todos) =>
    this._renderTodoCount(todos.length)
  );

  this._todoFilter = TodoAppFooterFiltersComponent.createFromTodoAppFooter(
    this
  );

  this._clearCompletedButton = this.getTarget().querySelector(
    ".clear-completed"
  );
  this._clearCompletedButton.addEventListener(
    "click",
    clickClearCompletedHandler.bind(this),
    true
  );
  function clickClearCompletedHandler() {
    this.getTarget().dispatchEvent(new CustomEvent("clear-completed"));
  }
}

TodoAppFooterComponent.prototype = Object.create(Component.prototype);

/**
 * @param {number} todoCount
 * @returns {TodoAppFooterComponent}
 */
TodoAppFooterComponent.prototype._renderTodoCount = function (todoCount) {
  this._todoCounter.innerText = todoCount;
  return this;
};

/**
 * @param {TodoAppComponent} todoApp
 * @returns {TodoAppFooterComponent}
 */
TodoAppFooterComponent.createFromTodoApp = (todoApp) => {
  const target = todoApp.getTarget().querySelector("footer.footer");
  return new TodoAppFooterComponent({
    target,
    props: todoApp.getProps(),
  });
};
