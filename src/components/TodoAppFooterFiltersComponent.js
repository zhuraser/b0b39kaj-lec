/**
 * @constructor
 * @extends Component
 * @param {{target: string|Node|Element|HTMLElement, props: {filter: Writable<string>}}} props
 */
function TodoAppFooterFiltersComponent(props) {
  Component.call(this, props);

  // init
  this._filterButtons = Array.from(this.getTarget().querySelectorAll("a"));
  this._filterButtons.forEach(registerToggleFilterHandler.bind(this));

  /**
   * @param {Node|Element|HTMLElement} element
   */
  function registerToggleFilterHandler(element) {
    element.addEventListener("click", toggleFilterHandler.bind(this), true);
  }

  /**
   * @param {MouseEvent} event
   */
  function toggleFilterHandler(event) {
    event.stopPropagation();
    event.preventDefault();

    /** @type {HTMLElement} */
    const target = event.target;
    this.getTarget().querySelector("a.selected").classList.remove("selected");
    target.classList.add("selected");

    const newFilter = target.innerText.toLowerCase();
    this.getProps().filter.set(newFilter);
  }
}

TodoAppFooterFiltersComponent.prototype = Object.create(Component.prototype);

/**
 * @param {TodoAppFooterComponent} todoAppFooter
 * @returns {TodoAppFooterFiltersComponent}
 */
TodoAppFooterFiltersComponent.createFromTodoAppFooter = (todoAppFooter) => {
  const target = todoAppFooter.getTarget().querySelector(".filters");
  return new TodoAppFooterFiltersComponent({
    target,
    props: { filter: todoAppFooter.getProps().filter },
  });
};
