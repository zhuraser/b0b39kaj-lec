const subscriberQueue = [];

function writable(value, start = noop) {
  let stop;
  const subscribers = [];

  function set(newValue) {
    if (safeNotEqual(value, newValue)) {
      value = newValue;
      if (stop) {
        // store is ready
        const runQueue = !subscriberQueue.length;
        for (let i = 0; i < subscribers.length; i += 1) {
          const s = subscribers[i];
          s[1]();
          subscriberQueue.push(s, value);
        }
        if (runQueue) {
          for (let i = 0; i < subscriberQueue.length; i += 2) {
            subscriberQueue[i][0](subscriberQueue[i + 1]);
          }
          subscriberQueue.length = 0;
        }
      }
    }
  }

  function update(fn) {
    set(fn(value));
  }

  function subscribe(run, invalidate = noop) {
    const subscriber = [run, invalidate];
    subscribers.push(subscriber);
    if (subscribers.length === 1) {
      stop = start(set) || noop;
    }
    run(value);

    return () => {
      const index = subscribers.indexOf(subscriber);
      if (index !== -1) {
        subscribers.splice(index, 1);
      }
      if (subscribers.length === 0) {
        stop();
        stop = null;
      }
    };
  }

  return {
    set,
    update,
    subscribe,
  };
}

function readable(value, start) {
  return {
    subscribe: writable(value, start).subscribe,
  };
}

function derived(stores, fn, initialValue = null) {
  const single = !Array.isArray(stores);
  const storesArray = single ? [stores] : stores;

  const auto = fn.length < 2;

  return readable(initialValue, (set) => {
    let inited = false;
    const values = [];

    let pending = 0;
    let cleanup = noop;

    const sync = () => {
      if (pending) {
        return;
      }
      cleanup();
      const result = fn(single ? values[0] : values, set);
      if (auto) {
        set(result);
      } else {
        cleanup = typeof result === "function" ? result : noop;
      }
    };

    const unsubscribers = storesArray.map((store, i) =>
      subscribe(
        store,
        (value) => {
          values[i] = value;
          pending &= ~(1 << i);
          if (inited) {
            sync();
          }
        },
        () => {
          pending |= 1 << i;
        }
      )
    );

    inited = true;
    sync();

    return function stop() {
      run_all(unsubscribers);
      cleanup();
    };
  });
}

function subscribe(store, ...callbacks) {
  if (store == null) {
    return noop;
  }
  const unsub = store.subscribe(...callbacks);
  return unsub.unsubscribe ? () => unsub.unsubscribe() : unsub;
}

function get_store_value(store) {
  let value;
  subscribe(store, (_) => (value = _))();
  return value;
}
