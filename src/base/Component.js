/**
 * @constructor
 * @param {{target: string|Node|Element|HTMLElement, props: object}} props
 */
function Component(props) {
  let target = props.target;
  if (typeof target === "string") {
    target = document.querySelector(target);
  }
  if (!target) {
    throw "Target does not exist!";
  }

  this._target = target;
  this._props = Object.assign(
    {},
    props.props || {} /* , (target || {}).dataset */
  );
}

/**
 * @returns {Node|Element|HTMLElement}
 */
Component.prototype.getTarget = function () {
  return this._target;
};

/**
 * @returns {object}
 */
Component.prototype.getProps = function () {
  return this._props;
};

/**
 * @returns {void}
 */
Component.prototype.destroy = function () {
  const parent = this._target.parentElement;
  if (parent) {
    parent.removeChild(this._target);
  }
};
