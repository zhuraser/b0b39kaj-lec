function noop() {}

function safeNotEqual(a, b) {
  return a != a
    ? b == b
    : a !== b || (a && typeof a === "object") || typeof a === "function";
}

function run(fn) {
  return fn();
}

function run_all(fns) {
  fns.forEach(run);
}
