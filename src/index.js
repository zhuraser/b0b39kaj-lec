const ready = () => {
  if (document.body) {
    new TodoAppComponent({ target: "section.todoapp", props: state });
    return;
  }

  window.requestAnimationFrame(ready);
};
window.requestAnimationFrame(ready);
