/**
 * @param {{text: string, completed?: boolean}} todo
 */
function Todo(todo) {
  this._textStore = writable(todo.text);
  this._completedStore = writable(!!todo.completed);
}

Object.defineProperty(Todo.prototype, "text", {
  get: function () {
    return this._textStore.subscribe;
  },
});

Object.defineProperty(Todo.prototype, "isCompleted", {
  get: function () {
    return get_store_value(this._completedStore);
  },
});

Object.defineProperty(Todo.prototype, "completed", {
  get: function () {
    return this._completedStore.subscribe;
  },
  set: function (value) {
    this._completedStore.set(value);
  },
});
