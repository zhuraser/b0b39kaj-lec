/**
 * @param {Todo[]} todoList
 */
function TodoList(todoList) {
  const store = writable(todoList);

  /**
   * @param {Todo} todo
   */
  function addTodo(todo) {
    store.update((todos) => (todos.push(todo), todos));
  }

  function clearCompleted() {
    store.update((todos) => todos.filter((todo) => !todo.isCompleted));
  }

  return { subscribe: store.subscribe, addTodo, clearCompleted };
}
