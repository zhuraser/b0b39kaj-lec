#!/usr/bin/env sh

set -x

SRC_DIR_NAME="${SRC_DIR_NAME:-src}"
DIST_DIR_NAME="${DIST_DIR_NAME:-public}"

TMP_FILE="/tmp/$$"

printf "// @ts-check\n'use strict';(function(){\n" > ${TMP_FILE}

for FILE in $(find "${SRC_DIR_NAME}" -name "*.js" -type f)
do
  cat "$FILE" >> ${TMP_FILE}
done

printf "})();" >> ${TMP_FILE}

cp "${TMP_FILE}" "${DIST_DIR_NAME}/index.js"
rm "${TMP_FILE}"
