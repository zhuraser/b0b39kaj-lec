#!/usr/bin/env sh

set -x


BIN="${PWD}/node_modules/.bin"

if [ -e "${BIN}/terser" ];
then
	find public/ -type f \
		-name "*.js" ! -name "*.min.*" ! -name "vfs_fonts*" \
		-exec "${BIN}/terser" --compress --mangle -o {}.min {} \; \
		-exec mv {}.min {} \;
fi

if [ -e "${BIN}/html-minifier-terser" ];
then
	find public/ -type f \
		-name "*.html" \
		-exec "${BIN}/html-minifier-terser" --collapse-whitespace --remove-comments --remove-optional-tags --remove-redundant-attributes --remove-script-type-attributes --remove-tag-whitespace --use-short-doctype --minify-css true --minify-js true -o {}.min {} \; \
		-exec mv {}.min {} \;
fi

if [ -e "${BIN}/uglifycss" ];
then
	find public/ -type f \
		-name "*.css" ! -name "*.min.*" \
		-exec "${BIN}/uglifycss" --output {}.min {} \; \
		-exec mv {}.min {} \;
fi
