#!/usr/bin/env sh

set -x

STATIC_DIR_NAME="${STATIC_DIR_NAME:-static}"
DIST_DIR_NAME="${DIST_DIR_NAME:-public}"

cp -TRr "${PWD}/${STATIC_DIR_NAME}/" "${PWD}/${DIST_DIR_NAME}/"
