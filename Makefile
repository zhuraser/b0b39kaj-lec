
.PHONY: build
build: copy-static compile-js minify


.PHONY: dev
dev: build-dev open

.PHONY: build-dev
build-dev: copy-static compile-js


.PHONY: copy-static
copy-static:
	@./scripts/copy-static.sh

.PHONY: compile-js
compile-js:
	@./scripts/compile-js.sh

.PHONY: minify
minify:
	@./scripts/minify.sh


.PHONY: open
open:
	@xdg-open 2>&1 >/dev/null ./public/index.html &
